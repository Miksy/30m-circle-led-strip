# 30m Circle LED Strip

Cercle LED de 30 mètres de circonférence.\
Depuis un ordinateur on envoie, pour chaque LED, les informations de couleur RGB. Ces données sont envoyées à l'ESP32 en packets UDP via le Wifi.\
Ensuite l'ESP32, lit les paquets UDP et applique les couleurs aux Leds.

## Matériel

-   ordinateur
-   routeur wifi
-   microcontroleur ESP32
-   buck converter LM2596
-   6 bandeaux LED 12v WS2815 de 5 mètres avec 60 leds par mètre
-   alimentation 12v

## Upload du code sur l'ESP

Avant d'uploader le code sur l'ESP, il faut changer le SSID et le mot de passe avec ceux de votre Wifi.

```
const char* ssid = "NOM_DU_WIFI";
const char* pass = "MOT_DE_PASSE_DU_WIFI";
```

Bon à savoir:

-   les ESP ne peuvent pas se connecter à du wifi 5ghz.
-   il vaut mieux débrancher l'alim et les bandeaux de led quand vous updatez le code de l'ESP.

## Schéma

À la construction ou au montage, il faut bien faire attention au sens des bandeaux (le sens de la donnée, indiquée par une flèche sur le bandeau LED).

![schéma](https://gitlab.com/Miksy/30m-circle-led-strip/-/raw/main/sch%C3%A9ma.png)

## Ce qu'il faut envoyer depuis l'ordinateur

Le MTU de l'udp avec l'esp est limité à 1500 bytes càd qu'il faut envoyer des messages udp avec maximum 1500 bytes (avec les headers, donc 1472 sans) [plus de détail ici](https://stackoverflow.com/questions/14993000/the-most-reliable-and-efficient-udp-packet-size) \
On a 6 bandeaux leds de 5m avec 60 leds par mètre. Soit 6*5*60 = 1800 leds. Pour avoir un bon taux de rafraichissement (fps élevé), il est conseillé de ne pas mettre plus de 1000 leds sur la même pin de l'ESP, d'où les 2 lignes sur la carte. [Chaque led met 30 microsecondes à s'updater](https://quinled.info/2021/03/23/max-amount-of-addressable-leds/) \
Pour chaque led on envoie les couleurs R, G et B en nombre entier 8 bits = 1 octet = 1 byte. On a donc en tout 1800 leds \* 3 bytes de couleurs = 5400 bytes

Pour tomber en dessous de la limite du MTU, j'ai divisé en 4 portions. (du coup chaque portion couvre 450 leds, soit 1 bandeau et demi) \
5400 bytes / 4 = 1350 bytes par portion \
Et on rajoute 1 byte au début du message pour différencier à quelle portion du cercle appartient les données. (cf schéma ci-dessus)

Si on résume, à chaque frame on envoie 4 messages UDP. \
Chaque message est constitué comme ceci :
1er byte (portion) | 2e byte | 3e byte | 4e byte | 5e byte | 6e byte | 7e byte | 8e byte | ... | 1349e byte | 1350e byte | 1351e byte\
---|---|---|---|---|---|----|---|---|---|---|---
0, 1, 2 ou 3 | led1 R | led1 G | led1 B | led2 R | led2 G | led2 B | led3 B | ... | led450 R | led450 G | led450 B

J'ai ajouté au repo comme exemple le fichier .toe qui est une compo Touchdesigner.

Voilà et si tu veux que ça marche avec seulement 25 mètres, tu envoies exactement la même chose mais tu débranches un bandeau.
