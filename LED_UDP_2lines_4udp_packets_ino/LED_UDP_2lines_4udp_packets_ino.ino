#include "WiFi.h"
#include "AsyncUDP.h"
#include <FastLED.h>

// pinout, depends of printed circuit board
#define PIN1 27
#define PIN2 33

#define UDP_TX_PACKET_MAX_SIZE 1000

byte udpBuffer[UDP_TX_PACKET_MAX_SIZE];

// number of pixels per segment (pixels can be repeated and mirrored over led strip, so there might be more physical led)
// this is how much pixels are sent over the network. Multiply by 3 to get byte length
#define ledCOUNT 900

// pixels are duplicated and mirrored along LED strips
CRGB ledLine1[ledCOUNT];
CRGB ledLine2[ledCOUNT];

const char* ssid = "NOM_DU_WIFI";
const char* pass = "MOT_DE_PASSE_DU_WIFI";

AsyncUDP udp;

// Set your Static IP address
IPAddress local_IP(192, 168, 1, 199);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

void setup()
{
  //Serial.begin(115200);
  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet)){
    //Serial.println("STA Failed to configure");
  }

  //Serial.println("coucou");

  // connect
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    //Serial.print(".");
  }

  //Serial.print("wifi connected");

  // set-up LED
  FastLED.addLeds<WS2812B, PIN1, RGB>(ledLine1, ledCOUNT);
  FastLED.addLeds<WS2812B, PIN2, RGB>(ledLine2, ledCOUNT);
  
  if (udp.listen(1234)) {
    
    udp.onPacket([](AsyncUDPPacket packet) {
      
      byte check = *(packet.data());

      if(check == 0 || check == 1) {
        
        int offset = ledCOUNT * check / 2;
        for (int i=1;i<packet.length();i+=3) {

            ledLine1[(i - 1)/3 + offset].setRGB( *(packet.data()+i),
                                    *(packet.data()+i+1),
                                    *(packet.data()+i+2));
          
        }

      } else if (check == 2 || check == 3) {

        int offset = ledCOUNT * (check-2) / 2;
        for (int i=1;i<packet.length();i+=3) {

            ledLine2[(i - 1)/3 + offset].setRGB( *(packet.data()+i),
                                    *(packet.data()+i+1),
                                    *(packet.data()+i+2));
          
        }
      }

    });      
  }
}

void loop()
{
  FastLED.show();
}
